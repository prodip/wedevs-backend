<?php 
include_once '../vendor/autoload.php';

use App\Classes\Product;
use App\Classes\Category;
use App\Helpers\Session;
use App\Helpers\Format;

$product = new Product();
$category = new Category();
$format = new Format();

$products = $product->getProductList();
$category = $category->getCategories();
$baseUrl = $format->baseUrl();

?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <?php if(Session::has('success_message')) : ?>
              <h5 class="text-center">
                <span class='text-success'>
                  <?php
                      echo Session::get('success_message');
                      Session::destroy_session_value('success_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>

            <?php if(Session::has('error_message')) : ?>
              <h5 class="text-center">
                <span class='text-danger'>
                  <?php
                      echo Session::get('error_message');
                      Session::destroy_session_value('error_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Product List</h3>
              </div>
              <div class="card-body">
                <a href="add-product.php" class="btn btn-info mb-2">Add New Product</a>
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>SI</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price(Tk)</th>
                    <th>Created_at</th>
                    <th style="width: 80px;">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                  if(isset($products)) :
                  $i = 01;  
                  foreach($products as $key=>$product): 
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td>
                      <img src="<?php echo $baseUrl . $product['image'] ?>" width="200"/>
                    </td>
                    <td><?php echo $product['name'] ?></td>
                    <td><?php echo $product['category_name'] ?></td>
                    <td><?php echo $product['price'] ?></td>
                    <td><?php echo $format->formatDate($product['created_at']); ?></td>
                    <td>
                    <a href="view-product.php?id=<?php echo $product['id'];?>" class="btn btn-info btn-block btn-sm"><i class="fa fa-eye"></i> View</a>
                    <a href="edit-product.php?id=<?php echo $product['id'];?>" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i> Edit</a>
                    <a href="delete-product.php?id=<?php echo $product['id'];?>" class="btn btn-danger btn-block btn-sm" onclick="return confirm('Are you sure you want to delete this?');"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                  </tr>
                  <?php 
                  $i++;
                  endforeach; 
                  endif;
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->

  <script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>