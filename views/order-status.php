<?php 
include_once '../vendor/autoload.php';

use App\Classes\Order;

$order = new Order();

if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'order-list.php';</script>";
} else {
  $id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}
$mOrder = $order->status($id);

// echo '<pre>';
// print_r($mOrder);
// exit();


?>
