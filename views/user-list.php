<?php 
include_once '../vendor/autoload.php';

use App\Classes\User;
use App\Helpers\Session;
use App\Helpers\Format;

$user = new User();
$format = new Format();

$users = $user->getUserList();
// echo '<pre>';
// print_r($users);
// exit();
$baseUrl = $format->baseUrl();

?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <?php if(Session::has('success_message')) : ?>
              <h5 class="text-center">
                <span class='text-success'>
                  <?php
                      echo Session::get('success_message');
                      Session::destroy_session_value('success_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>

            <?php if(Session::has('error_message')) : ?>
              <h5 class="text-center">
                <span class='text-danger'>
                  <?php
                      echo Session::get('error_message');
                      Session::destroy_session_value('error_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">User List</h3>
              </div>
              <div class="card-body">
                <a href="add-user.php" class="btn btn-info mb-2">Add New User</a>
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>SI</th>
                    <th>Avatar</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Created_at</th>
                    <th style="width: 80px;">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                  if(isset($users)) :
                  foreach($users as $key=>$user): 
                  ?>
                  <tr>
                    <td><?php echo ($key + 1); ?></td>
                    <td>
                      <?php if(!empty($user['avatar'])) : ?>
                      <img src="<?php echo $baseUrl . $user['avatar'] ?>" width="200"/>
                      <?php else : ?>
                      <img src="" width="200"/>
                      <?php endif; ?>
                    </td>
                    <td><?php echo $user['uname']; ?></td>
                    <td><?php echo $user['uemail']; ?></td>
                    <td><?php echo $user['uphone']; ?></td>
                    <td><?php echo $format->formatDate($user['created_at']); ?></td>
                    <td>
                    <a href="edit-user.php?id=<?php echo $user['id'];?>" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i> Edit</a>
                    <a href="delete-user.php?id=<?php echo $user['id'];?>" class="btn btn-danger btn-block btn-sm" onclick="return confirm('Are you sure you want to delete this?');"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                  </tr>
                  <?php 
                  endforeach; 
                  endif;
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->

  <script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>