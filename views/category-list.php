<?php 
include_once '../vendor/autoload.php';

use App\Classes\Category;
use App\Helpers\Session;

$category = new Category();

$categories = $category->getCategories();

?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <?php if(Session::has('success_message')) : ?>
            <h5 class="text-center">
            <span class='text-success'>
            <?php
                echo Session::get('success_message');
              //  Session::destroy_session_value('success_message');
            ?>
            </span>
            </h5>
            <?php endif; ?>

            <?php if(Session::has('error_message')) : ?>
            <h5 class="text-center">
            <span class='text-error'>
            <?php
                echo Session::get('error_message');
              //  Session::destroy_session_value('error_message');
            ?>
            </span>
            </h5>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">

          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Category List</h3>
              </div>
              <div class="card-body">
                <a href="add-category.php" class="btn btn-info mb-2">Add New Category</a>
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>SI</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                  foreach($categories as $key=>$category): 
                  ?>
                  <tr>
                    <td><?php echo ($key+1); ?></td>
                    <td><?php echo $category['name'] ?></td>
                    <td><?php echo $category['status'] ?></td>
                    <td>
                    <a href="edit-category.php?id=<?php echo $category['id'];?>" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i> Edit</a>
                    <a href="delete-category.php?id=<?php echo $category['id'];?>" class="btn btn-danger btn-block btn-sm" onclick="return confirm('Are you sure you want to delete this?');"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                  </tr>
                  <?php 
                  endforeach; 
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->