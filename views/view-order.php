<?php 
include_once '../vendor/autoload.php';

use App\Classes\Order;
use App\Helpers\Session;

$order = new Order();

if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'order-list.php';</script>";
} else {
  $id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}
$mOrder = $order->show($id);

?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <?php if(Session::has('success_message')) : ?>
              <h5 class="text-center">
                <span class='text-success'>
                  <?php
                      echo Session::get('success_message');
                      Session::destroy_session_value('success_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>

            <?php if(Session::has('error_message')) : ?>
              <h5 class="text-center">
                <span class='text-danger'>
                  <?php
                      echo Session::get('error_message');
                      Session::destroy_session_value('error_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
          <div class="card">
              <div class="card-header">
              <a href="order-list.php" class="float-left mr-2"><i class="fa fa-arrow-left"></i></a>
                <h3 class="card-title">Order view</h3>
              </div>
              <div class="card-body">
              <div class="row">
              <div class="col-md-6 float-left">
              <h5>Order ID: <?php echo $mOrder['id']; ?></h5>
              <h6>Date: <?php echo date('F j, Y', strtotime($mOrder['created_at'])); ?></h6>
              </div>
              <div class="col-md-6 float-left text-right">
              <h6>Customer Name: <?php echo $mOrder['user']['username']; ?></h6>
              <?php if(!empty( $mOrder['user']['userphone'])) : ?>
              <h6>Customer Phone: <?php echo $mOrder['user']['userphone']; ?></h6>
              <?php endif ?>
              </div>
              </div>
                
                <table id="example3" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>SI</th>
                    <th>Product</th>
                    <th> Qty</th>
                    <th> Price($)</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $t_qty = 0;
                  $t_price = 0;
                  if(count($mOrder['cart']) > 0) :
                  $i = 01;  
                  foreach($mOrder['cart'] as $product): 
                    $t_qty += $product['quantity'];
                    $t_price += $product['price'];
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $product['product_name']; ?></td>
                    <td><?php echo $product['quantity']; ?></td>
                    <td><?php echo '$'.$product['price']; ?></td>
                  </tr>
                  <?php 
                  $i++;
                  endforeach; 
                  endif;
                  ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <td colspan="2"></td>
                    <td>Total Qty : <?php echo $t_qty; ?></td>
                    <td>Total Price : <?php echo '$'.$t_price; ?></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->
