<?php 
include_once '../vendor/autoload.php';

use App\Classes\Category;

$category = new Category();
if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'category-list.php';</script>";
} else {
  $catId = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}

$category->delete($catId);

?>
