<?php 
include_once '../vendor/autoload.php';

use App\Classes\User;

$user = new User();
if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'user-list.php';</script>";
} else {
  $userId = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}

$user->delete($userId);

?>
