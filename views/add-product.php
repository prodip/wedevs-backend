<?php 
include_once '../vendor/autoload.php';

use App\Classes\Product;
use App\Classes\Category;
use App\Helpers\Session;

$product = new Product();
$category = new Category();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $product->store($_POST, $_FILES);
}
$categories = $category->getCategories();
?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-10">
            <?php if(Session::has('success_message')) : ?>
              <h5 class="text-center">
                <span class='text-success'>
                  <?php
                      echo Session::get('success_message');
                      Session::destroy_session_value('success_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>

            <?php if(Session::has('error_message')) : ?>
            <h5 class="text-center">
              <span class='text-danger'>
                <?php
                    echo Session::get('error_message');
                    Session::destroy_session_value('error_message');
                ?>
              </span>
            </h5>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-10">
            <div class="card">
              <div class="card-header">
                <a href="product-list.php" class="float-left mr-2"><i class="fa fa-arrow-left"></i></a>
                <h3 class="card-title">Add <small>New Product</small></h3>
              </div>
              <form id="quickForm" action="" method="post" enctype="multipart/form-data">
                <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputName">Name</label>
                  <input type="text" name="name" class="form-control" id="exampleInputName" placeholder="Product Title">
                </div>
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control select2 select2-info" name="category"  id="exampleInputCategory" data-dropdown-css-class="select2-info" style="width: 100%;">
                    <option value="">Select Category</option>
                    <?php foreach($categories as $category) : ?>
                    <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="summernote">Description</label>
                  <textarea class="form-control required" id="summernote" name="description"></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputPrice">Price</label>
                  <input type="number" name="price" class="form-control" id="exampleInputPrice" placeholder="Price">
                </div>
                <div class="form-group">
                  <label for="customFile">Upload File</label>
                  <div class="custom-file">
                    <input type="file" name="mfile" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>
                </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->

<script>
$(function () {
  //Initialize Select2 Elements
  $('.select2').select2()

  $('#summernote').summernote({
    height:300
  })

  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      name: {
        required: true,
        name: true,
      },
      description: {
        required: true,
        description: true,
      },
      category: {
        required: true,
      },
      price: {
        required: true,
        price: true,
      },
      file: {
        required: true,
        file: true
      },
    },
    messages: {
      name: {
        required: "Please enter a name",
        name: "Please enter a product name"
      },
      description: {
        required: "Please provide a description",
        description: "Please provide product description"
      },
      category: "Please select a category",
      price: "Please provide a price",
      file: "Please upload a product image"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>