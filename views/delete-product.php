<?php 
include_once '../vendor/autoload.php';

use App\Classes\Product;

$product = new Product();
if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'product-list.php';</script>";
} else {
  $prodId = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}

$product->delete($prodId);

?>
