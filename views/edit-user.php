<?php 
include_once '../vendor/autoload.php';

use App\Classes\User;
use App\Helpers\Session;

$user = new User();
if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'user-list.php';</script>";
} else {
  $userId = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $user->update($_POST, $userId);
}

$mUser = $user->edit($userId);

?>

<?php include_once('inc/header.php'); ?>

<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-10">
            <h5 class="text-center">
            <span class='text-success'>
            <?php
            if (Session::has('success_message')) {
                echo Session::get('success_message');
                Session::destroy_session_value('success_message');
            }
            ?>
            </span>
            </h5>
           
            <h5 class="text-center">
            <span class='text-danger'>
            <?php
            if (Session::has('error_message')) {
              echo Session::get('error_message');
              Session::destroy_session_value('error_message');
            }
            ?>
            </span>
            </h5>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-10">
            <div class="card">
              <div class="card-header">
              <a href="user-list.php" class="float-left mr-2"><i class="fa fa-arrow-left"></i></a>
                <h3 class="card-title">Edit <small> User</small></h3>
              </div>
              <form id="quickForm" action="" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName" value="<?php echo $mUser['uname'];?>" placeholder="User Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputName">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail" value="<?php echo $mUser['uemail'];?>" placeholder="User Email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputName">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="User Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputName">Phone</label>
                    <input type="text" name="phone" class="form-control" id="exampleInputPhone" value="<?php echo $mUser['uphone'];?>" placeholder="User Phone">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputStatus">Gender</label>
                    <select class="form-control select2 select2-info" name="gender"  id="exampleInputGender" data-dropdown-css-class="select2-primary">
                      <option value="male" <?php if($mUser['ugender'] == 'male'){echo 'selected';} ?>>Male</option>
                      <option value="female" <?php if($mUser['ugender'] == 'female'){echo 'selected';} ?>>Female</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputStatus">Status</label>
                    <select class="form-control select2 select2-info" name="status"  id="exampleInputStatus" data-dropdown-css-class="select2-info">
                      <option value="active" <?php if($mUser['ustatus'] == 'active'){echo 'selected';} ?>>Active</option>
                      <option value="inactive" <?php if($mUser['ustatus'] == 'inactive'){echo 'selected';} ?>>Inactive</option>
                    </select>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->