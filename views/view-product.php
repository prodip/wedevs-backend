<?php 
include_once '../vendor/autoload.php';

use App\Classes\Product;
use App\Classes\Category;
use App\Helpers\Format;

$product = new Product();
$category = new Category();
$format = new Format();

$baseUrl = $format->baseUrl();

if (!isset($_GET['id']) || $_GET['id'] == null) {
  echo "<script>window.location = 'product-list.php';</script>";
} else {
  $prodId = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['id']);
}
$mProduct = $product->show($prodId);
?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12 my-4">
            <div class="card">
              <div class="card-header">
                <a href="product-list.php" class="float-left mr-2"><i class="fa fa-arrow-left"></i></a><h3 class="card-title">View <small> Product</small></h3>
              </div>

              <div class="col-md-12 my-4">
                <div class="col-md-6 float-left">
                <img src="<?php echo $baseUrl . $mProduct['image'] ?>" class="mt-3" width="200"/>
                </div>
                <div class="col-md-6 float-left">
                  <p>Title: <?php echo $mProduct['name'] ?></p>
                  <p>Category: <?php echo $mProduct['name'] ?></p>
                  <p>Price: <?php echo $mProduct['name'] ?></p>
                </div>
              </div>

              <div class="col-md-12">
              <h5><strong> Product Description</strong></h5>
              <div class="desc">
              <?php echo $mProduct['description'] ?>
              </div>
              </div>

 
            </div>
            </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->