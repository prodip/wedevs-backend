<?php 
include_once '../vendor/autoload.php';

 use App\Classes\Category;
 use App\Helpers\Session;

 $category = new Category();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $category->store($_POST);
}

?>

<?php include_once('inc/header.php'); ?>

<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h5>
            <span class='text-success'>
            <?php
            if (Session::has('success_message')) {
                echo Session::get('success_message');
                Session::destroy_session_value('success_message');
            }
            ?>
            </span>
            </h5>
           
            <h5>
            <span class='text-error'>
            <?php
            if (Session::has('error_message')) {
                echo Session::get('error_message');
                Session::destroy_session_value('error_message');
            }
            ?>
            </span>
            </h5>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-10">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add <small> Category</small></h3>
              </div>
              <form id="quickForm" action="" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName" placeholder="Category Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputStatus">Status</label>
                    <select class="form-control select2 select2-info" name="status"  id="exampleInputStatus" data-dropdown-css-class="select2-info">
                      <option value="active">Active</option>
                      <option value="inactive">Inactive</option>
                    </select>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->