<?php 
include_once '../vendor/autoload.php';

use App\Classes\Order;
use App\Helpers\Session;

$orders = new Order();
$orders = $orders->getOrderList();
// echo '<pre>';
// print_r($orders);
// exit();

?>

<?php include_once('inc/header.php'); ?>
<div class="wrapper">
  <!-- Navbar -->
  <?php include_once('inc/navbar.php'); ?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <?php include_once('inc/sidebar.php'); ?>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <?php if(Session::has('success_message')) : ?>
              <h5 class="text-center">
                <span class='text-success'>
                  <?php
                      echo Session::get('success_message');
                      Session::destroy_session_value('success_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>

            <?php if(Session::has('error_message')) : ?>
              <h5 class="text-center">
                <span class='text-danger'>
                  <?php
                      echo Session::get('error_message');
                      Session::destroy_session_value('error_message');
                  ?>
                </span>
              </h5>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Order List</h3>
              </div>
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>SI</th>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Total Qty</th>
                    <th>Total Price</th>
                    <th>Status</th>
                    <th style="width: 80px;">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                  if(isset($orders)) :
                  $i = 01;  
                  foreach($orders as $key=>$order): 
                  ?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $order['id']; ?></td>
                    <td><?php echo $order['user']['username']; ?></td>
                    <td><?php echo $order['user']['useremail']; ?></td>
                    <td><?php echo $order['cart']['total_quantity']; ?></td>
                    <td><?php echo $order['cart']['total_price']; ?></td>
                    <td><?php echo $order['status']; ?></td>
                    <td>
                    <a href="view-order.php?id=<?php echo $order['id'];?>" class="btn btn-info btn-block btn-sm"><i class="fa fa-eye"></i> View</a>
                    <?php if($order['status'] == 'pending') : ?>
                    <a href="order-status.php?id=<?php echo $order['id'];?>" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i> Delivered</a>
                    <?php else : ?>
                      <a href="order-status.php?id=<?php echo $order['id'];?>" class="btn btn-primary btn-block btn-sm"><i class="fa fa-edit"></i> Pending</a>
                    <?php endif ?>
                    </td>
                  </tr>
                  <?php 
                  $i++;
                  endforeach; 
                  endif;
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /.content-wrapper -->

  <!-- Footer -->
  <?php include_once('inc/footer.php'); ?>
  <!-- /.Footer -->

  <script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>