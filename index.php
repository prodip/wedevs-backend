<?php 
include_once 'vendor/autoload.php';

use App\Classes\Admin;
use App\Helpers\Session;

Session::init();
Session::checkLogin();
$admin = new Admin();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $loginChk= $admin->login($_POST);
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE | Log in</title>
  <link rel="icon" href="public/img/favicon.png" type="image/png" sizes="32x32">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="public/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="public/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="public/index2.html"><b>Login</b></a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <?php 
     // print_r($_SESSION);
       ?>
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <?php if(Session::has('success_message')) : ?>
                <h5 class="text-center">
                  <span class='text-success'>
                    <?php
                        echo Session::get('success_message');
                        Session::destroy_session_value('success_message');
                    ?>
                  </span>
                </h5>
              <?php endif; ?>

              <?php if(Session::has('error_message')) : ?>
                <h5 class="text-center">
                  <span class='text-danger'>
                    <?php
                        echo Session::get('error_message');
                        Session::destroy_session_value('error_message');
                    ?>
                  </span>
                </h5>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </section>

      <p class="login-box-msg">Sign in to start your session</p>
      <form action="" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- jQuery -->
<script src="public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="public/js/adminlte.min.js"></script>
</body>
</html>
