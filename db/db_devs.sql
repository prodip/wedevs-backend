-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2021 at 01:03 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_devs`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(10) NOT NULL,
  `aname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `aemail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `apass` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `astatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `aname`, `aemail`, `apass`, `avatar`, `astatus`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'active', '2021-04-25 23:00:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `order_id`, `product_id`, `product_name`, `quantity`, `price`, `created_at`) VALUES
(1, 1, 5, 'Product Five', 2, 7000, '2021-04-30 16:01:59'),
(2, 1, 4, 'Product Four', 1, 2500, '2021-04-30 16:01:59'),
(3, 1, 1, 'Product One', 3, 2550, '2021-04-30 16:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', 'active', '2021-04-30 15:19:08', NULL),
(2, 'Clothing', 'active', '2021-04-30 15:20:47', NULL),
(3, 'Shoes', 'active', '2021-04-30 15:21:20', NULL),
(4, 'Fashion', 'active', '2021-04-30 15:21:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending',
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `user_id`, `status`, `created_at`) VALUES
(1, 2, 'pending', '2021-04-30 16:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(5) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `name`, `sku`, `category`, `description`, `price`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Product One', '608bcdf1d417a', 3, '<p><span style=\"font-family: Roboto, sans-serif; font-size: 14px; background-color: rgb(255, 255, 0);\"><font color=\"#000000\" style=\"font-family: Roboto, Bangla634, sans-serif;\">Lorem ipsum dolor sit amet</font></span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span></p><p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <b><u>Duis aute irure</u></b> dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span></p><p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla634, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla634, sans-serif;\"></span><br></p><p><br></p>', 850, 'uploads/products/prod-1-1619774961.jpg', '2021-04-30 15:29:21', NULL),
(2, 'Product Two', '608bce4014756', 3, '<p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in<b> reprehenderit in voluptate velit esse cillum </b>dolore eu fugiat nulla pariatur.</span></p><p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span></p><p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla179, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla179, sans-serif;\"></span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla179, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla179, sans-serif;\"></span><br></p>', 650, 'uploads/products/prod-5-1619775040.jpg', '2021-04-30 15:30:40', NULL),
(3, 'Product Three', '608bcec56f73a', 3, '<p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span></p><p><span style=\"font-family: Roboto, sans-serif; font-size: 14px;\"><font color=\"#363432\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure </font><b style=\"\"><font color=\"#000000\" style=\"background-color: rgb(255, 255, 0);\">dolor in reprehenderit</font></b><font color=\"#363432\"> in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</font></span></p><p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla198, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla198, sans-serif;\"></span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla198, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla198, sans-serif;\"></span><br></p>', 750, 'uploads/products/prod-4-1619775173.jpg', '2021-04-30 15:32:53', NULL),
(4, 'Product Four', '608bcf7b68756', 2, '                  <p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span></p><p><span style=\"font-family: Roboto, sans-serif; font-size: 14px;\"><b style=\"\"><font color=\"#000000\" style=\"background-color: rgb(255, 255, 0);\">Lorem ipsum dolor</font></b><font color=\"#363432\"> sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</font></span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla220, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla220, sans-serif;\"></span><br></p>                  ', 2500, 'uploads/products/product1-1619775355.jpg', '2021-04-30 15:35:55', NULL),
(5, 'Product Five', '608bcff741fbe', 2, '<p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\"><b>Lorem ipsum dolor sit amet, consectetur adipi</b>sicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span></p><p><span style=\"color: rgb(54, 52, 50); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span><span style=\"color: rgb(54, 52, 50); font-family: Roboto, Bangla722, sans-serif; font-size: 14px;\"><br style=\"font-family: Roboto, Bangla722, sans-serif;\"></span><br></p>', 3500, 'uploads/products/product4-1619775479.jpg', '2021-04-30 15:37:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(10) NOT NULL,
  `uname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `uemail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `upass` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `uphone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ugender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ustatus` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `uname`, `uemail`, `upass`, `uphone`, `ugender`, `avatar`, `_token`, `ustatus`, `created_at`, `updated_at`) VALUES
(1, 'demo one', 'demo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '01926813104', 'male', NULL, '097283e3bb7f1d4553921d381dc12116', 'active', '2021-04-30 15:41:41', NULL),
(2, 'Shozib Hasan', 'shozib@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, 'c0156445e3adbd2ec1a018289d7664e7', 'active', '2021-04-30 16:00:26', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aemail` (`aemail`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uemail` (`uemail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
