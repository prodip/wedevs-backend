<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6f7a8b217972edffb55a7ab25d3c772a
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'App\\Classes\\BaseClass' => __DIR__ . '/../..' . '/app/Classes/BaseClass.php',
        'App\\Classes\\Product' => __DIR__ . '/../..' . '/app/Classes/Product.php',
        'App\\Classes\\UserClass' => __DIR__ . '/../..' . '/app/Classes/UserClass.php',
        'App\\Classes\\utility' => __DIR__ . '/../..' . '/app/Classes/utility.php',
        'App\\Helpers\\Database' => __DIR__ . '/../..' . '/app/Helpers/Database.php',
        'App\\Helpers\\Format' => __DIR__ . '/../..' . '/app/Helpers/Format.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6f7a8b217972edffb55a7ab25d3c772a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6f7a8b217972edffb55a7ab25d3c772a::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit6f7a8b217972edffb55a7ab25d3c772a::$classMap;

        }, null, ClassLoader::class);
    }
}
