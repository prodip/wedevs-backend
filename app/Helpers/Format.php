<?php
namespace App\Helpers;
/**
 * Format Class
 */
class Format
{
    public function baseUrl(){
        if($_SERVER['SERVER_NAME'] == "localhost"){
            $baseUrl = 'http://' . $_SERVER['SERVER_NAME']. '/weDevs-backend/';
            return $baseUrl;
		}else{
            $baseUrl = $_SERVER['SERVER_NAME'];
            return $baseUrl;
		}
    }
    public function formatDate($date)
    {
        return date('F j, Y, g:i a', strtotime($date));
    }

    public function sinceDate($date)
    {
        return date('j, Y', strtotime($date));
    }

    public function textShorten($text, $limit = 400)
    {
        $text = $text . " ";
        $text = substr($text, 0, $limit);
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text . ".....";
        return $text;
    }

    public function validation($data)
    {
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function title()
    {
        $path  = $_SERVER['SCRIPT_FILENAME'];
        $title = basename($path, '.php');
        //$title = str_replace('_', ' ', $title);
        if ($title == 'index') {
            $title = 'home';
        } elseif ($title == 'contact') {
            $title = 'contact';
        }
        return $title = ucfirst($title);
    }
}