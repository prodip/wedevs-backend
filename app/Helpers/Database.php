<?php
namespace App\Helpers;

Class Database{
	//  public $host   = DB_HOST;
	//  public $user   = DB_USER;
	//  public $pass   = DB_PASS;
	//  public $dbname = DB_NAME;

    public $host   = "localhost";
    public $user   = "root";
    public $pass   = "";
    public $dbname = "db_devs";
	 
	 
	 public $conn;
	 public $error;
	 
	 public function __construct(){
	  $this->connDB();
	 }
	 
	private function connDB(){
		 $this->conn = mysqli_connect($this->host, $this->user, $this->pass,$this->dbname);
		 if(!$this->conn){
		   $this->error ="Connection fail".mysqli_connect_error();
		  return false;
		 }
	 }

	public function getHost(){
		if($_SERVER['SERVER_NAME'] == "localhost"){
			define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']. 'weDevs-backend/');
		}else{
			define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']);
		}
	}
	 
	// Select or Read data
	public function select($query){
	  $result = $this->conn->query($query) or die($this->conn->error.__LINE__);
		  if($result->num_rows > 0){
		    return $result;
		  }else {
		    return false;
		  }
	 }
 
	// Insert data
	public function insert($query){
		 $insert_row = $this->conn->query($query) or die($this->conn->error.__LINE__);
		 if($insert_row){
		   return $insert_row;
		 } else {
		   return false;
		 }
	 }
	  
	// Update data
	 public function update($query){
	 $update_row = $this->conn->query($query) or die($this->conn->error.__LINE__);
		 if($update_row){
		  return $update_row;
		 } else {
		  return false;
		  }
	 }
	  
	// Delete data
	 public function delete($query){
	 $delete_row = $this->conn->query($query) or die($this->conn->error.__LINE__);
		 if($delete_row){
		   return $delete_row;
		 }else{
		   return false;
		}
	 }

 //end of class 
}