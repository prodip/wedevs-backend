<?php
namespace App\Classes;

use App\Helpers\Database;
use App\Helpers\Format;
use App\Helpers\Session;

class Category
{
	private $db;
	private $fm;
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function getCategories(){
		$sql = "SELECT tbl_category.* FROM tbl_category";
		$result = $this->db->select($sql);
		if ($result) {
			return $result;
		}else{
			$msg = "Category not found !";
			return $msg;
		}
	}

	public function store($data){
		$catName = $this->fm->validation($data['name']);
		$catName = mysqli_real_escape_string($this->db->conn, $catName);
		$catStatus = $this->fm->validation($data['status']);
		$catStatus = mysqli_real_escape_string($this->db->conn, $catStatus);
		if (empty($catName) or empty($catStatus)){
			Session::set('error_message', "Fields must not be empty !");
		}else{
			$query = "INSERT INTO tbl_category(name,status) VALUES('$catName','$catStatus')";
			$inserted = $this->db->insert($query);
			if ($inserted) {
				Session::set('success_message', "Category Save Successfully!");
			}else{
				Session::set('error_message', "Failed to insert!");
			}
		}
		header('location:category-list.php');
	}

	public function edit($catId){
		$query = "SELECT * FROM tbl_category WHERE id = '$catId'";
		$category = $this->db->select($query);
		return $category;
	}

	public function update($data,  $catId){
		$catName = $this->fm->validation($data['name']);
		$catName = mysqli_real_escape_string($this->db->conn, $catName);
		$catStatus = $this->fm->validation($data['status']);
		$catStatus = mysqli_real_escape_string($this->db->conn, $catStatus);
		if (empty($catName) or empty($catStatus)){
			Session::set('error_message', "Fields must not be empty !");
		}else{
			$query = "UPDATE tbl_category
						SET
						name     	= '$catName',
						status 		= '$catStatus'
						WHERE id    = '$catId'";
			$updated = $this->db->update($query);
			
			if ($updated) {
				Session::set('success_message', "Category Updated Successfully!");
			}else{
				Session::set('error_message', "Failed to update!");
			}
		}
		header('location:category-list.php');
	}

	public function delete($id)
    {
        $id = mysqli_real_escape_string($this->db->conn, $id);
        $query = "DELETE FROM tbl_category WHERE id = '$id'";
        $deldata = $this->db->delete($query);
		if ($deldata) {
			Session::set('success_message', "Category Deleted Successfully!");
		}else{
			Session::set('error_message', "Category Not Deleted!");
		}
		header('location:category-list.php');
    }

}
?>