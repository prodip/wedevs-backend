<?php 
namespace App\Classes;

use App\Helpers\Database;
use App\Helpers\Session;
use App\Helpers\Format;

Class Admin {

    private $db;
	private $fm;

    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function getAdmin($id){
        $query = "SELECT * FROM tbl_admin WHERE id = '$id'";
		$admin = $this->db->select($query);
		return $admin;
    }

    public function login($data)
    {
        $adminUser = $this->fm->validation($data['email']);
        $adminPass = $this->fm->validation($data['password']);
        $adminUser = mysqli_real_escape_string($this->db->conn, $adminUser);
        $adminPass = mysqli_real_escape_string($this->db->conn, $adminPass);

        if (empty($adminUser) || empty($adminPass)) {
			Session::set('error_message', "Username or Password must not be empty!");
            echo "<script>window.top.location='index.php'</script>";
        } else {
            $query = "SELECT * FROM tbl_admin WHERE aemail = '$adminUser' AND apass = md5('$adminPass')";
            $result = $this->db->select($query);
            if ($result != false) {
                $user = $result->fetch_assoc();
                Session::set("adminlogin", true);
                Session::set("adminId", $user['id']);
                Session::set("adminName", $user['aname']);
                header("Location:views/dashboard.php");
            } else {
                Session::set('error_message', "Username or Password not match!");
                header("Location: index.php");
            }
        }
    }

    public function signout(){
        Session::init();
        Session::destroy();
    }

}




?>