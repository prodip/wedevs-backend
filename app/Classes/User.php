<?php
namespace App\Classes;

use App\Helpers\Database;
use App\Helpers\Session;
use App\Helpers\Format;

class User {

    private $db;
	private $fm;
    
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function getUserList(){
		$sql = "SELECT *
				FROM tbl_user
				ORDER BY tbl_user.id DESC ";
		$result = $this->db->select($sql);
		if ($result) {
			return $result;
		}
	}

    public function getUserByToken($token){
		$query = "SELECT * FROM tbl_user WHERE _token = '$token'";
		$user = $this->db->select($query);
		return $user->fetch_assoc();
	}
    
    public function store($data){
        $this->dataValidator($data);
        $name=$data['name'];
        $email=$data['email'];
        $password=$data['password'];
        $phone=$data['phone'];
        $gender=$data['gender'];
        $status=$data['status'];
        $token =md5(uniqid(rand(), true));
        
        if (empty($name) or empty($email) or empty($password) or empty($gender)){
			Session::set('error_message', "Fields must not be empty!");
			header('location:add-user.php');
		}else{
            $queryCheck="SELECT * FROM tbl_user WHERE uemail='$email'";
            $mQuery = $this->db->select($queryCheck);
            if(!empty($mQuery)){
                $_SESSION['error_message']="Email already Exist, please choose another email For SignUp";
                echo "<script>window.top.location='add-user.php'</script>";
            }else{
                $query = "INSERT INTO tbl_user(uname,uemail,upass,uphone,ugender,_token,ustatus) 
				VALUES('$name','$email',md5('$password'),'$phone','$gender', '$token','$status')";
                $inserted = $this->db->insert($query);
                if ($inserted) {
                    Session::set('success_message', "User Saved successfully!");
                    header('location:user-list.php');
                }else{
                    Session::set('error_message', "Failed to insert!");
                    header('location:add-user.php');
                }
            }
		} 
    }

    public function dataValidator($data){
        if($data['name']){
            $name = $this->fm->validation($data['name']);
            $name = mysqli_real_escape_string($this->db->conn, $name);
        }
        
        if($data['email']){
        $email = $this->fm->validation($data['email']);
		$email = mysqli_real_escape_string($this->db->conn, $email);
        }

        if($data['password']){
        $password = $this->fm->validation($data['password']);
		$password = mysqli_real_escape_string($this->db->conn, $password);
        }

        if($data['phone']){
        $phone = $this->fm->validation($data['phone']);
		$phone = mysqli_real_escape_string($this->db->conn, $phone);
        }

        if($data['status']){
        $status = $this->fm->validation($data['status']);
		$status = mysqli_real_escape_string($this->db->conn, $status);
        }
        return $data;
    }

    public function edit($id){
		$query = "SELECT * FROM tbl_user WHERE id = '$id'";
		$user = $this->db->select($query);
		return $user->fetch_assoc();
	}

    public function update($data, $id){
        $data = $this->dataValidator($data);
        $name=$data['name'];
        $email=$data['email'];
        $phone=$data['phone'];
        $gender=$data['gender'];
        $status=$data['status'];
        
        if (empty($name) or empty($email) or empty($gender)){
			Session::set('error_message', "Fields must not be empty!");
			header('location:add-user.php');
		}else{
            $queryCheck="SELECT * FROM tbl_user WHERE uemail='$email' AND id != $id";
            $mQuery = $this->db->select($queryCheck);
            if(!empty($mQuery)){
                $_SESSION['error_message']="Email already Exist, please choose another email For SignUp";
                echo "<script>window.top.location='add-user.php'</script>";
            }else{

                $query = "UPDATE tbl_user
							SET 
							uname     	   = '$name',
							uemail         = '$email',
							uphone         = '$phone',
							ugender        = '$gender',
							ustatus        = '$status'
							WHERE id       = '$id'";
				$updated = $this->db->update($query);
				if ($updated) {
					Session::set('success_message', "User updated successfully!");
                    echo "<script>window.top.location='user-list.php'</script>";
				}else{
					Session::set('success_message', "Failed to updated!");
                    echo "<script>window.top.location='edit-user.php'</script>";
				}

            }
		} 
    }

    public function delete($id){
        $id = mysqli_real_escape_string($this->db->conn, $id);
        $query = "DELETE FROM tbl_user WHERE id = '$id'";
        $deldata = $this->db->delete($query);
		if ($deldata) {
			Session::set('success_message', "User Deleted Successfully!");
		}else{
			Session::set('error_message', "User Not Deleted!");
		}
		header('location:user-list.php');
    }

    public function apiLogin($data){

        if (! $this->validatePerson($data)) {
            return $this->unprocessableEntityResponse();
        }
        $useremail = $data['useremail'];
        $password = $data['password'];

        $queryCheck="SELECT * FROM tbl_user WHERE uemail='$useremail' AND upass=md5('$password')";
        $result = $this->db->select($queryCheck);

        if (! $result) {
            return $this->notFoundResponse();
        }
        $result = $result->fetch_assoc();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['userdata'] = $result;
        return json_encode($response);
		
    }

    public function apiSignup($data){
      
        if (! $this->validatePerson($data)) {
            return $this->unprocessableEntityResponse();
        }
        $name = $data['username'];
        $email = $data['useremail'];
        $password = $data['password'];
        $token =md5(uniqid(rand(), true));

        $check = $this->emailCheck($email);
        if($check){
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response['error'] = 'Email already Exist!';
            return json_encode($response);
        }

        $query = "INSERT INTO tbl_user(uname,uemail,upass,_token) 
        VALUES('$name','$email',md5('$password'),'$token')";
        $result = $this->db->insert($query);

        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['userdata'] = 'Sign Up Completed. Please Login';
        return json_encode($response);
		
    }

    private function emailCheck($email){
        $queryCheck="SELECT * FROM tbl_user WHERE uemail='$email'";
        $result = $this->db->select($queryCheck);
        return $result;
        
    }

    private function validatePerson($input)
    {
        if (! isset($input['useremail'])) {
            return false;
        }
        if (! isset($input['password'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = [
            'error' => 'Invalid input'
        ];
        return json_encode($response);
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return json_encode($response);
    }

    
    
    
}
