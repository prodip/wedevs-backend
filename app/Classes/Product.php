<?php
namespace App\Classes;

use App\Helpers\Database;
use App\Helpers\Format;
use App\Helpers\Session;
use Exception;

class Product
{
	private $db;
	private $fm;
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
		$this->db->getHost();
	}

	public function getProductList(){
		$sql = "SELECT tbl_product.*, tbl_category.name as category_name
				FROM tbl_product
				INNER JOIN tbl_category
				ON tbl_product.category = tbl_category.id
				ORDER BY tbl_product.id DESC ";
		$result = $this->db->select($sql);
		if ($result) {
			return $result;
		}
	}

	public function show($id){
		$pid = $this->fm->validation($id);
		$pid = mysqli_real_escape_string($this->db->conn, $pid);
		$sql = "SELECT tbl_product.*
				 FROM tbl_product 
				 WHERE id = '$pid'";
		$result = $this->db->select($sql);
		return $result->fetch_assoc();
	}

	public function store($data, $file){

		$productName = $this->fm->validation($data['name']);
		$productName = mysqli_real_escape_string($this->db->conn, $productName);

		$p_category = $this->fm->validation($data['category']);
		$p_category = mysqli_real_escape_string($this->db->conn, $p_category);

		$p_description = $data['description'];
		$p_description = mysqli_real_escape_string($this->db->conn, $p_description);

		$p_price = $this->fm->validation($data['price']);
		$p_price = mysqli_real_escape_string($this->db->conn, $p_price);
		$p_uid = uniqid();

		$file_name = $file['mfile']['name'];

		if (empty($productName) or empty($p_category) or empty($p_description) or empty($p_price) or empty($file_name))
		{
			Session::set('error_message', "Fields must not be empty!");
			header('location:add-product.php');
		}else{
			$uploaded_image = $this->uploadFile($file);
			$query = "INSERT INTO tbl_product(name,sku,category,description,price,image) 
				VALUES('$productName','$p_uid','$p_category','$p_description','$p_price','$uploaded_image')";
			$inserted = $this->db->insert($query);
			if ($inserted) {
				Session::set('success_message', "Product inserted successfully!");
				header('location:product-list.php');

			}else{
				Session::set('error_message', "Failed to insert!");
				header('location:add-product.php');

			}
		} 
	}


	public function uploadFile($file){
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
		$file_temp = $file['mfile']['tmp_name'];
		$file_name = $file['mfile']['name'];
		$fileNameArr = explode('.', $file_name);
		$file_ext = strtolower(end($fileNameArr));		
		$fileNm = strtolower(array_shift($fileNameArr));
		$file_name = $fileNm .'-'.time() .'.'.$file_ext;
		
		if(is_uploaded_file($file_temp)){
		if (in_array($file_ext, $permited) === false) {
			Session::set('error_message', "You can upload only:-".implode(', ', $permited));
			header('location:add-product.php');
		}else{
			try{
				$uploaded_image = 'uploads/products/'.$file_name;
				$uploads_path = ROOT_PATH. $uploaded_image;
				move_uploaded_file($file_temp, $uploads_path);
				return $uploaded_image;
			}catch(Exception $e){
				echo $e->getMessage();
			}
		}
		}else{
			Session::set('error_message', "Upload file not fould!");
			header('location:add-product.php');
		}
	}
	
	//get Product by ID
	public function edit($id){
		$pid = $this->fm->validation($id);
		$pid = mysqli_real_escape_string($this->db->conn, $pid);
		$sql = "SELECT tbl_product.*
				 FROM tbl_product 
				 WHERE id = '$pid'";
		$result = $this->db->select($sql);
		return $result->fetch_assoc();
	}

	//update product
	public function update($data, $file, $id){
		$productName = $this->fm->validation($data['name']);
		$productName = mysqli_real_escape_string($this->db->conn, $productName);

		$p_category = $this->fm->validation($data['category']);
		$p_category = mysqli_real_escape_string($this->db->conn, $p_category);

		$p_description = $data['description'];
		$p_description = mysqli_real_escape_string($this->db->conn, $p_description);

		$p_price = $this->fm->validation($data['price']);
		$p_price = mysqli_real_escape_string($this->db->conn, $p_price);

		if (empty($productName) or empty($p_category) or empty($p_description) or empty($p_price))
		{
			Session::set('error_message', "Fields must not be empty!");
			header('location:edit-product.php');
		}else{

			if (!empty($file['mfile']['name'])) {
				$uploaded_image = $this->uploadFile($file);
				$query = "UPDATE tbl_product
							SET 
							name     	  = '$productName',
							category      = '$p_category',
							description   = '$p_description',
							price         = '$p_price',
							image         = '$uploaded_image'
							WHERE id      = '$id'";
				$updated = $this->db->update($query);
				if ($updated) {
					Session::set('success_message', "Product updated successfully!");
					header('location:product-list.php');
				}else{
					Session::set('success_message', "Failed to updated!");
					header('location:edit-product.php');
				}
			}else{
				$query = "UPDATE tbl_product
							SET 
							name     	  = '$productName',
							category      = '$p_category',
							description   = '$p_description',
							price         = '$p_price'
							WHERE id      = '$id'";
				$updated = $this->db->update($query);
				if ($updated) {
					Session::set('success_message', "Product updated successfully!");
					header('location:product-list.php');
				}else{
					Session::set('success_message', "Failed to updated!");
					header('location:edit-product.php');
				}
			}
		}
	}

	public function delete($id)
    {
		$product = $this->edit($id);
        $id = mysqli_real_escape_string($this->db->conn, $id);
        $query = "DELETE FROM tbl_product WHERE id = '$id'";
        $deldata = $this->db->delete($query);
		$uploads_path = ROOT_PATH. $product['image'];
		if ($deldata) {
			Session::set('success_message', "Product Deleted Successfully!");
			unlink($uploads_path);
		}else{
			Session::set('error_message', "Product Not Deleted!");
		}
		header('location:product-list.php');
    }

	//add product to compare table
	public function insertCompare($csid,$prodId){
		$ckSql = "SELECT * FROM tbl_compare WHERE csId='$csid' AND productId='$prodId'";
		$getCmp = $this->db->select($ckSql);
		if ($getCmp) {
			$msg = "<span class='error'>Already added.</span>";
				return $msg;
		}else{
		$sql = "SELECT * FROM tbl_product WHERE pid = '$prodId' ";
		$result = $this->db->select($sql)->fetch_assoc();
		if ($result) {
			$productName = $result['productName'];
			$price = $result['price'];
			$image = $result['image'];
			$query = "INSERT INTO tbl_compare(csId,productId,productName,price,image)VALUES('$csid','$prodId','$productName','$price','$image')";
				$inserted = $this->db->insert($query);
				if ($inserted) {
					$msg = "<span class='success'>Added to Compare.</span>";
					return $msg;
				}else{
					$msg = "<span class='error'>Failed to Added.</span>";
					return $msg;
				}
			}
		}
	}
	//get Compared data by Customer Id
	public function getComparedData($csid){
		$sql = "SELECT * FROM tbl_compare WHERE csId ='$csid'";
		$result = $this->db->select($sql);
		return $result;
	}
	//check compare table data
	public function checkCompareData($csid){
		$sql = "SELECT * FROM tbl_compare WHERE csId = '$csid'";
		$result = $this->db->select($sql);
		return $result;
	}
	//delete compare data when logout
	public function delCustomerCompare($csid){
		$sql = "DELETE FROM tbl_compare WHERE csId = '$csid'";
		$result = $this->db->delete($sql);
	}
	//save product to wishlist
	public function saveToWishlist($csid,$pdid){
		$ckSql = "SELECT * FROM tbl_wishlist WHERE csId='$csid' AND productId='$pdid'";
		$getwlist = $this->db->select($ckSql);
		if ($getwlist) {
			$msg = "<span class='error'>Already added to list.</span>";
				return $msg;
		}else{
		$sql = "SELECT * FROM tbl_product WHERE pid = '$pdid' ";
		$result = $this->db->select($sql)->fetch_assoc();
		if ($result) {
			$productName = $result['productName'];
			$price = $result['price'];
			$image = $result['image'];
			$query = "INSERT INTO tbl_wishlist(csId,productId,productName,price,image)VALUES('$csid','$pdid','$productName','$price','$image')";
				$inserted = $this->db->insert($query);
				if ($inserted) {
					$msg = "<span class='success'>Added ! Check wishlist page.</span>";
					return $msg;
				}else{
					$msg = "<span class='error'>Failed to Added.</span>";
					return $msg;
				}
			}
		}
	}
	//check wishlist data
	public function checkWishlistData($csid){
		$sql = "SELECT * FROM tbl_wishlist WHERE csId = '$csid'";
		$result = $this->db->select($sql);
		return $result;
	}
	//delete compare data when logout
	public function delCustomerwishlist($csid, $pdid){
		$sql = "DELETE FROM tbl_wishlist WHERE csId = '$csid' AND productId='$pdid'";
		$result = $this->db->delete($sql);
		if ($result) {
			$msg = "<span class='success'>Wishlist product deleted.</span>";
			return $msg;
		}else{
			$msg = "<span class='error'>Failed to delete.</span>";
			return $msg;
		}
	}


	public function apiGetProducts(){
		$sql = "SELECT tbl_product.*, tbl_category.name as category_name
		FROM tbl_product
		INNER JOIN tbl_category
		ON tbl_product.category = tbl_category.id
		ORDER BY tbl_product.id DESC ";
		$result = $this->db->select($sql);

        if (! $result) {
            return $this->notFoundResponse();
        }
		$result = $result->fetch_all(MYSQLI_ASSOC);

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['products'] = $result;
        return json_encode($response);
		
    }

	public function apiGetProduct($id){
		$pid = $this->fm->validation($id);
		$pid = mysqli_real_escape_string($this->db->conn, $pid);
		$sql = "SELECT tbl_product.*, tbl_category.name as category_name
				 FROM tbl_product 
				 INNER JOIN tbl_category
				 ON tbl_product.category = tbl_category.id
				 WHERE tbl_product.id = '$pid'";
		$result = $this->db->select($sql);
		$result = $result->fetch_assoc();
		$response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['product'] = $result;
        return json_encode($response);
	}

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return json_encode($response);
    }

}
?>