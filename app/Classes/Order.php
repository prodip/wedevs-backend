<?php
namespace App\Classes;

use App\Helpers\Database;
use App\Helpers\Session;
use App\Helpers\Format;
use App\Classes\User;

class Order {

    private $db;
	private $fm;
    
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function show($id){
		$id = $this->fm->validation($id);
		$id = mysqli_real_escape_string($this->db->conn, $id);

        $sql = "SELECT * FROM tbl_order AS o WHERE o.id=$id";
		$orders = $this->db->select($sql);
        $orders = $orders->fetch_all(MYSQLI_ASSOC);
        $result = '';
        if(count($orders) > 0){
            foreach($orders as $order){
                $order_id = $order['id'];
                $user_id = $order['user_id'];
                $sql1 = "SELECT c.* FROM tbl_cart AS c WHERE c.order_id=$order_id";
		        $cart = $this->db->select($sql1);
                $cart = $cart->fetch_all(MYSQLI_ASSOC);
                $order['cart'] = $cart;

                

                $sql2 = "SELECT u.uname as username, u.uemail as useremail, u.uphone as userphone  FROM tbl_user AS u WHERE u.id=$user_id";
		        $user = $this->db->select($sql2);
                $user = $user->fetch_assoc();
                $order['user'] = $user;
                $result = $order;

            }
        }

        if ($result) {
			return $result;
		}
	}

    public function status($id){
		$id = $this->fm->validation($id);
		$id = mysqli_real_escape_string($this->db->conn, $id);
        $sql = "SELECT * FROM tbl_order AS o WHERE o.id=$id";
		$order = $this->db->select($sql);
        $order = $order->fetch_assoc();
        if($order['status'] == 'pending'){
            $query = "UPDATE tbl_order
                        SET 
                        status     	  = 'delivered'
                        WHERE id      = $id";
            $updated = $this->db->update($query);
        }else{
            $query = "UPDATE tbl_order
                        SET 
                        status     	  = 'pending'
                        WHERE id      = $id";
            $updated = $this->db->update($query);
        }

        if ($updated) {
            Session::set('success_message', "Order Status Updated!");
            header('location:order-list.php');
        }else{
            Session::set('success_message', "Failed to updated!");
            header('location:order-list.php');
        }
	}

    public function getOrderList(){

        $result = [];
        $sql = "SELECT * FROM tbl_order AS o ORDER BY o.id DESC";
		$orders = $this->db->select($sql);
        if($orders){
        $orders = $orders->fetch_all(MYSQLI_ASSOC);
        
        if(count($orders) > 0){
            foreach($orders as $order){
                $order_id = $order['id'];
                $user_id = $order['user_id'];
                $sql1 = "SELECT SUM(c.quantity) AS total_quantity, SUM(c.price) AS total_price FROM tbl_cart AS c WHERE c.order_id=$order_id";
		        $cart = $this->db->select($sql1);
                $cart = $cart->fetch_assoc();
                $order['cart'] = $cart;

                $sql2 = "SELECT u.uname as username, u.uemail as useremail  FROM tbl_user AS u WHERE u.id=$user_id";
		        $user = $this->db->select($sql2);
                $user = $user->fetch_assoc();
                $order['user'] = $user;
                $result[] = $order;

            }
        }
        }

		if ($result) {
			return $result;
		}
	}

    public function apiStoreOrder($orders){

        $user = new User();
        $mUser = $user->getUserByToken($orders['_token']);
        if (! $this->validatePerson($orders) || ! $mUser) {
            return $this->unprocessableEntityResponse();
        }
        $query = "INSERT INTO tbl_order(user_id) VALUES('$mUser[id]')";
		$this->db->insert($query);
        $order_id = $this->db->conn->insert_id;

        $result ='';
        foreach($orders['cart'] as $item){
            $product_id = $item['product']['id'];
            $product_name = $item['product']['name'];
            $quantity = $item['quantity'];
            $price = $item['tPrice'];
            $query = "INSERT INTO tbl_cart(order_id,product_id,product_name,quantity,price) 
				VALUES('$order_id','$product_id','$product_name','$quantity','$price')";
			$result = $this->db->insert($query);
        }  

        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['order_store'] = $result;
        return json_encode($response);
		
    }

    private function validatePerson($input)
    {
        if (! isset($input['_token'])) {
            return false;
        }
        if (! isset($input['cart'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = [
            'error' => 'Invalid input'
        ];
        return json_encode($response);
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return json_encode($response);
    }

    
    
    
}
